var NAVTREE =
[
  [ "Magnetic calibration", "index.html", [
    [ "Kalibrierung von Magnetfeldsensoren zur Indoorlokalisierung", "index.html", [
      [ "Einleitung", "index.html#einleitung", null ],
      [ "Kontext", "index.html#Kontext", null ],
      [ "Problematik", "index.html#probs", null ],
      [ "Kalibrierung", "index.html#kalib", [
        [ "Fehlerbeschreibung", "index.html#fehler", [
          [ "Hard-Iron Fehler", "index.html#hir", null ],
          [ "Noise", "index.html#noise", null ],
          [ "Soft-Iron Fehler", "index.html#sif", null ],
          [ "Skalierungsfehler", "index.html#sf", null ],
          [ "Ausrichtungsfehler", "index.html#af", null ]
        ] ]
      ] ],
      [ "Evaluierung", "index.html#eva", null ],
      [ "Referenzen", "index.html#refe", null ]
    ] ],
    [ "Anleitung zur Kalibrierung", "anwendung.html", [
      [ "Vorbereitung", "anwendung.html#vorb", null ],
      [ "Durchführung", "anwendung.html#durchf", [
        [ "calibration_tool", "anwendung.html#ct", null ],
        [ "Verarbeitung calibration_tool", "anwendung.html#vct", null ],
        [ "calibration_tool_helmholtz", "anwendung.html#vhs", null ]
      ] ]
    ] ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"anwendung.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';