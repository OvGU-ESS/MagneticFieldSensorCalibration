#! /usr/bin/env python

## @package calibration_tool_helmholtz
# The calibration_tool_helmholtz can be used to find the correct measurementsraw_to_compensated.rangeCalibration1 values for the the \link<measurementsraw_to_compensated>measurementsraw_to_compensated\endlink script.
# 
# Therefore it starts a ROS node. The node subscribes the "magneticMeasurementsraw" topic. The 
# measurementsraw_to_compensated.rangeCalibration1 values are calculated by using the received data 
# and the calibration values. The calibration values are calculated by using the
# \link<calibration_tool>calibration_tool\endlink script.
# 
# Explanation on how to use this script and which data is needed for what and calculated were 
#is given at page \ref anwendung "Bedienungsanleitung".

import rospy
from magnetic_maps.msg import MyMessage
import numpy as np
from numpy.linalg import inv
import tf

## The offset calibration values for the sensor array you want to calibrate. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset = [[19.5, -109.5, 20.0], 
           [44.5, -72.0, 1.5], 
           [9.0, -102.5, 87.5], 
           [-75.5, -125.0, -37.5], 
           [4.0, -44.0, 165.0], 
           [9.0, -246.0, -214.5], 
           [29.0, -18.5, 116.5], 
           [29.0, -140.0, -53.5], 
           [47.5, -62.0, 110.0], 
           [-21.0, -132.5, 2.0]]

## The initial range calibration values for the sensor array you want to calibrate. 
# They will be multiplied to the raw measurements, after adding the calibration_tool_helmholtz.offset. 
# They make a sphere from the ellipsoid. For every sensor are 3 values given, for every axis one. 
# 
# This values also scale your measurements. In the case of calibration_tool_helmholtz this mean, 
# you get values without a unit.
rangeCalibration = [[0.0018365472910927456, 0.0017683465959328027, 0.001976284584980237], 
                     [0.0018298261665141812, 0.0017271157167530224, 0.0019436345966958211], 
                     [0.0018281535648994515, 0.0017683465959328027, 0.0019860973187686196], 
                     [0.0018604651162790699, 0.0017699115044247787, 0.0018570102135561746], 
                     [0.0017857142857142857, 0.0017761989342806395, 0.0019193857965451055], 
                     [0.0018450184501845018, 0.0017953321364452424, 0.0019249278152069298], 
                     [0.0018248175182481751, 0.00176522506619594, 0.0019361084220716361], 
                     [0.0018115942028985507, 0.0017605633802816902, 0.0021390374331550803], 
                     [0.0018001800180018001, 0.0017543859649122807, 0.001953125], 
                     [0.0018450184501845018, 0.0018001800180018001, 0.0019607843137254902]]

## The rotation compensation values for the sensor array you want to calibrate. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration3. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation = [[0.0, 0.0, 0.0, 0.0], 
                         [-0.0015532326821163496, -0.036234825958357364, -0.011084780238338457, 0.99928061749594166], 
                         [-0.027023830935010859, -0.029114887564765792, 0.0016161877196519767, 0.99920939938580533], 
                         [0.033644883142042226, 0.0041018825678716423, -0.0058773719812349086, 0.99940815130573746], 
                         [-0.046766417090596132, -0.048473203611437365, -0.0050542136124548309, 0.99771624507618006], 
                         [-0.063704585786740611, -0.063090097891412594, -0.018955386467798398, 0.99579217642118889], 
                         [0.00029383343028517757, -0.065963879210673704, -0.015431986575701906, 0.99770262808700783], 
                         [-0.068951487583968685, -0.071181273577469936, -0.0086290056663902541, 0.99503992830089816], 
                         [-0.036336275150428525, -0.05323858559886855, -0.48921429868831778, 0.86977853392143545], 
                         [-0.057833570279054294, -0.030634533356791224, 0.0093929758896435043, 0.99781189385511226]]

## Strength of the magnetic field in the helmholtz coil.
# You have to set this value
fieldstrength = 1.413

## The sensor which is actually in the coil
# You have to set this value.
numSensor = 0

## Average length of the measured vectors in this run.
averageLength = 0

## Sum of all length measured in this run.
sumLength = 0

## Counts the number of measurements are taken for this run.
counter = 0

## Calculates new measurementsraw_to_compensated.rangeCalibration1 values.
# The values are calculated for every sensor in a single run. For instructions on how to use this function
# to calibrate, see page \ref anwendung "Bedienungsanleitung".
# 
# The first step of this function is to calibrate the measured data with the calibration values you get from the
# \link<calibration_tool>calibration_tool\endlink script. Than the average length in y direction of all 
# measured vectors is calculated. With this data the new calibration values are calculated.
# 
# <b>Input:</b> Data of type "MyMessage"
# 
# <b>Print:</b> The new measurementsraw_to_compensated.rangeCalibration1 values for the sensor 
# you put in the coil and defined in calibration_tool_helmholtz.numSensor.
def callback(daten):
    global counter
    global numSensor
    global averageLength
    global sumLength
    global rangeCalibration

    # calibration of the measurements
    for i in range(0,10):
        vec = np.matrix(((0.0, 0.0, 0.0, 1)))
        for j in range(3):
            val = float(daten.data[i*3+j])
            val = val - offset[i][j]
            val = val*rangeCalibration[i][j]
            vec[0,j] = val
        vec = np.transpose(vec)
        vec = inv(tf.transformations.quaternion_matrix(rotationCompensation[i])) * vec

        # calculating the scaleFactor
        if i==numSensor:
            counter = counter + 1
            # calculate the length of the measured vector
            length = abs(vec[1,0])

            sumLength = sumLength + length
            averageLength = sumLength / counter
            newScaleFactor = fieldstrength/averageLength
  
            scaleFactor = np.matrix(((0.0, 0.0, 0.0)))
            scaleFactor[0, 0] = newScaleFactor * rangeCalibration[i][0]
            scaleFactor[0, 1] = newScaleFactor * rangeCalibration[i][1]
            scaleFactor[0, 2] = newScaleFactor * rangeCalibration[i][2]
            rospy.loginfo(scaleFactor)

## listener
# Starts the subscriber.
# Node listen to topic "magneticMeasurementsraw" with the data type "MyMessage"
def listener():
    rospy.Subscriber('magneticMeasurementsraw', MyMessage, callback)
    rospy.spin()


if __name__ == '__main__':
    rospy.init_node('Listen_sensordata', anonymous=True)
    try:
        listener()

    except rospy.ROSInterruptException:
        print "something bad happend2"

