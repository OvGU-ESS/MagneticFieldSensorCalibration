#! /usr/bin/env python
import rospy
import serial
import time
import ctypes
from magnetic_maps.msg import MyMessage

ports = [serial.Serial('/dev/ttyUSB0', 57600,timeout=.1),
#         serial.Serial('/dev/ttyUSB4', 57600,timeout=.1),
#         serial.Serial('/dev/ttyUSB5', 57600,timeout=.1),
#         serial.Serial('/dev/ttyUSB6', 57600,timeout=.1),
#         serial.Serial('/dev/ttyUSB7', 57600,timeout=.1)
        ]

pub = rospy.Publisher('magneticMeasurementsraw', MyMessage, queue_size=110)

def gateway():
    #call all stripes   
    for port in ports:
        # send synch/start command to all avr's
        port.write("#s00")

    while not rospy.is_shutdown():
        for port in ports:
            daten = port.readline()

            if daten:
                rospy.loginfo(daten)
                # data is in form - [# ID sens1.x sens1.y sens1.z sens2.x sens2.y sens2.z ... ]
                # ID is the bar's number starting from 1 (the highst above ground) to 4 
                # the sensor data is in Hex format with alphabet excluding the symbol '#'
                if daten[0]=='#': 
                    output = MyMessage()
                    # Output is in Form [ID sens1.x sens1.y sens1.z sens2.x sens2.y sens2.z ... ]
                    # x, y and z are transformed to  int16 type 
                    output.level=int(daten[1])
                    for i in range(2,len(daten)-2,4):
                        output.data.append(ctypes.c_int16(int(str(daten[i:i+4]),16)).value)

                    output.header.stamp = rospy.Time.now()
                    pub.publish(output)


if __name__ == '__main__':
    rospy.init_node('read_sensorarray', anonymous=True)
    
    try:
        #wait for avr to calm
        time.sleep(2)
      
        #flush all garbage
        for port in ports:
            port.flushInput()
            port.flushOutput()

        ##wait for avr to calm
        time.sleep(2) 

        gateway()
    except rospy.ROSInterruptException:
        #  pass
        print "something bad happend"

