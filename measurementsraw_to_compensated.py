#! /usr/bin/env python

## @package measurementsraw_to_compensated
# The script measurementsraw_to_compensated.py calibrates the magnetic measurements.
# 
# Therefore it starts a ROS node. The node subscribes the "magneticMeasurementsraw" topic. The calibrated 
# data is calculated by using the received data and the calibration values. The calibration values are 
# calculated by using the \link<calibration_tool>calibration_tool\endlink script.

import rospy
from magnetic_maps.msg import MyMessage
from magnetic_maps.msg import MagneticCalibrated
from magnetic_maps.msg import Magnetic
import numpy as np
from numpy.linalg import inv
import tf

## The number of sensors on the sensor array you want to calibrate
numSensors = 10

## The offset calibration values for sensor array 1. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset1 = [[127.0, -49.5, -272.0], 
           [-46.0, -11.5, -221.5], 
           [61.0, 23.0, -190.5], 
           [-16.0, -48.5, -210.5], 
           [38.5, 53.0, 24.0], 
           [-81.5, -208.5, -501.0], 
           [-11.0, 204.0, -156.0], 
           [14.5, -13.0, -378.0], 
           [-15.5, 74.5, -185.5], 
           [101.0, 103.5, -57.0]]

## The range calibration values for sensor array 1. 
# They will be multiplied to the raw measurements, after adding the offset1. They make a sphere from the 
# ellipsoid. For every sensor are 3 values given, for every axis one. This values also scale your 
# measurements. This mean, you get values in a unit. For example in Gauss.
rangeCalibration1 = [[ 0.00100754,  0.00117378,  0.00122632], 
                     [ 0.0009256,   0.00089224,  0.00097566], 
                     [ 0.00090407,  0.00089185,  0.00095909], 
                     [ 0.00092265,  0.00087409,  0.00095031], 
                     [ 0.00091672,  0.00168861,  0.00105367], 
                     [ 0.00084523,  0.00082763,  0.00093803], 
                     [ 0.00093518,  0.00088349,  0.00095378], 
                     [ 0.00090202,  0.00089055,  0.00096585], 
                     [ 0.00092892,  0.00092566,  0.0009192 ], 
                     [ 0.00081996,  0.00092753,  0.00110516]]

## The rotation compensation values for sensor array 1. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration1. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation1 = [[  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   0.00000000e+00],
                         [ -6.21250271e-04,  -4.79253635e-03,  -1.74855015e-03,   9.98442213e-01],
                         [  1.74936636e-03,  -6.58804654e-03,  -8.41279704e-03,   9.98445721e-01],
                         [ -6.50894131e-03,  -6.18631500e-03,  -5.77623874e-04,   9.98489682e-01],
                         [ -3.42243375e-03,   1.94299681e-03,  -6.12338301e-03,   9.98405680e-01],
                         [ -6.74290993e-04,  -1.21682319e-02,  -9.39535336e-03,   9.98310529e-01],
                         [ -7.15434417e-03,  -6.18295792e-03,  -4.36162430e-04,   9.98318462e-01],
                         [ -3.80760870e-03,  -1.43913926e-02,   5.83712869e-03,   9.98226214e-01],
                         [ -8.55270667e-05,  -9.69610950e-03,  -7.20483096e-03,   9.97810913e-01],
                         [  1.17486482e-03,  -4.61692359e-03,  -1.54193829e-02,   9.98974901e-01]]


## The offset calibration values for sensor array 2. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset2 = [[-23.5, 117.5, -31.5], 
           [27.0, 61.5, -61.0], 
           [-99.0, 100.0, -91.0], 
           [26.5, 76.5, -16.5], 
           [24.5, 128.0, 43.5], 
           [97.0, -27.5, -289.0], 
           [24.5, -104.0, -228.5], 
           [-9.0, 133.0, 130.0], 
           [-6.0, -25.5, -171.0], 
           [-84.5, -15.0, 70.0]]

## The range calibration values for sensor array 2. 
# They will be multiplied to the raw measurements, after adding the offset2. They make a sphere from the 
# ellipsoid. For every sensor are 3 values given, for every axis one. This values also scale your 
# measurements. This mean, you get values in a unit. For example in Gauss.
rangeCalibration2 = [[ 0.00091769,  0.00088125,  0.00098743], 
                     [ 0.000933,    0.0009067,   0.00102485], 
                     [ 0.00087518,  0.00105247,  0.00118321], 
                     [ 0.00098521,  0.00152528,  0.00113663], 
                     [ 0.00092288,  0.00091732,  0.00106913], 
                     [ 0.00078989,  0.00090312,  0.00097964], 
                     [ 0.00092668,  0.00088833,  0.00100649], 
                     [ 0.00096775,  0.00095092,  0.00085971], 
                     [ 0.00093837,  0.00087938,  0.0009678 ], 
                     [ 0.00077037,  0.00088495,  0.00091733]]

## The rotation compensation values for sensor array 2. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration2. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation2 = [[ 0.        ,  0.        ,  0.        ,  0.        ],
                         [ 0.00186864, -0.01016557,  0.01399294,  0.99966352],
                         [ 0.00269728,  0.01201033,  0.00854904,  0.99753091],
                         [ 0.00136757,  0.0038415 ,  0.00593139,  0.99828751],
                         [ 0.00241286, -0.01062861, -0.0130881 ,  0.99964121],
                         [ 0.00437601, -0.01182936, -0.01324135,  0.9970001 ],
                         [-0.00834546, -0.01095945, -0.01720137,  0.99944207],
                         [ 0.00798287, -0.0079796 , -0.00634998,  0.99587444],
                         [ 0.00804053, -0.00586374, -0.00312118,  0.99962708],
                         [-0.00186489, -0.00200399, -0.00661257,  0.99719788]]


## The offset calibration values for sensor array 3. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset3 = [[19.5, -109.5, 20.0], 
           [44.5, -72.0, 1.5], 
           [9.0, -102.5, 87.5], 
           [-75.5, -125.0, -37.5], 
           [4.0, -44.0, 165.0], 
           [9.0, -246.0, -214.5], 
           [29.0, -18.5, 116.5], 
           [29.0, -140.0, -53.5], 
           [47.5, -62.0, 110.0], 
           [-21.0, -132.5, 2.0]]

## The range calibration values for sensor array 3. 
# They will be multiplied to the raw measurements, after adding the offset3. They make a sphere from the 
# ellipsoid. For every sensor are 3 values given, for every axis one. This values also scale your 
# measurements. This mean, you get values in a unit. For example in Gauss.
rangeCalibration3 = [[ 0.00089168,  0.00085857,  0.00095953], 
                     [ 0.00089242,  0.00084233,  0.00094793], 
                     [ 0.00090629,  0.00087665,  0.00098459], 
                     [ 0.00088964,  0.00084634,  0.00088799], 
                     [ 0.00090278,  0.00089797,  0.00097036], 
                     [ 0.00093656,  0.00091134,  0.00097712], 
                     [ 0.00088448,  0.0008556 ,  0.00093842], 
                     [ 0.00094187,  0.00091533,  0.00111211], 
                     [ 0.00089062,  0.00086797,  0.00096629], 
                     [ 0.00095182,  0.00092869,  0.00101154]]

## The rotation compensation values for sensor array 3. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration3. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation3 = [[0.0, 0.0, 0.0, 0.0], 
                         [-0.0015532326821163496, -0.036234825958357364, -0.011084780238338457, 0.99928061749594166], 
                         [-0.027023830935010859, -0.029114887564765792, 0.0016161877196519767, 0.99920939938580533], 
                         [0.033644883142042226, 0.0041018825678716423, -0.0058773719812349086, 0.99940815130573746], 
                         [-0.046766417090596132, -0.048473203611437365, -0.0050542136124548309, 0.99771624507618006], 
                         [-0.063704585786740611, -0.063090097891412594, -0.018955386467798398, 0.99579217642118889], 
                         [0.00029383343028517757, -0.065963879210673704, -0.015431986575701906, 0.99770262808700783], 
                         [-0.068951487583968685, -0.071181273577469936, -0.0086290056663902541, 0.99503992830089816], 
                         [-0.036336275150428525, -0.05323858559886855, -0.48921429868831778, 0.86977853392143545], 
                         [-0.057833570279054294, -0.030634533356791224, 0.0093929758896435043, 0.99781189385511226]]


## The offset calibration values for sensor array 4. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset4 = [[44.5, 146.5, 167.5], 
           [-47.5, 160.0, 114.0], 
           [41.5, 170.5, 73.0], 
           [31.0, 152.0, 7.5], 
           [24.5, 162.5, 53.0], 
           [49.5, 142.5, 61.5], 
           [-52.5, 153.0, 122.5], 
           [33.5, 270.5, 135.5], 
           [74.5, 159.0, 99.5], 
           [39.5, 146.0, 254.5]]

## The range calibration values for sensor array 4. 
# They will be multiplied to the raw measurements, after adding the offset4. They make a sphere from the 
# ellipsoid. For every sensor are 3 values given, for every axis one. This values also scale your 
# measurements. This mean, you get values in a unit. For example in Gauss.
rangeCalibration4 = [[ 0.00084935,  0.00087708,  0.00089461], 
                     [ 0.00074509,  0.00087291,  0.00089028], 
                     [ 0.00085114,  0.00083663,  0.00090158], 
                     [ 0.00086956,  0.00086816,  0.00088462], 
                     [ 0.00087708,  0.00087277,  0.00089551], 
                     [ 0.00089737,  0.00088989,  0.00095181], 
                     [ 0.00076079,  0.00088634,  0.00090796], 
                     [ 0.00108222,  0.00089264,  0.00111635], 
                     [ 0.00080502,  0.00087299,  0.00089877], 
                     [ 0.00087203,  0.00088727,  0.00089396]]

## The rotation compensation values for sensor array 4. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration4. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation4 = [[  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   0.00000000e+00],
                         [  2.29632028e-03,   2.09417078e-02, -3.17028467e-02,   9.97008236e-01],
                         [ -2.20259342e-03,   7.54542077e-03,   1.76517483e-04,   9.99883955e-01],
                         [  1.07965781e-03,   7.96637702e-03,  -2.67035835e-03,   9.99864337e-01],
                         [ -5.00976643e-03,   8.54544919e-03,  -6.25386600e-03,   9.99805981e-01],
                         [  2.70955082e-04,   8.71818648e-03,  -5.14449798e-04,   9.99831490e-01],
                         [ -2.42448565e-03,   2.31355141e-02,  -3.41251710e-03,   9.98185095e-01],
                         [  2.84962665e-02,   5.22900803e-03,   7.46025204e-03,   9.96634481e-01],
                         [ -2.07613592e-03,   1.84634904e-03,  -2.06872405e-03,   9.99421589e-01],
                         [  1.25236489e-03,   4.00877508e-03,  -6.45511996e-04,   9.99774986e-01]]


## The offset calibration values for sensor array 5. 
# They will be added to the raw measurements. With them the center of the ellipsoid will be shifted to the 
# center of the sensor. For every sensor are 3 values given, for every axis one.
offset5 = [[99.5, 618.0, 651.5], 
           [137.0, 625.5, 561.0], 
           [54.0, 571.0, 538.5], 
           [-1.5, 220.0, 61.5], 
           [26.0, 133.0, -104.0], 
           [0.5, 310.0, 128.5], 
           [-33.0, 2.0, -188.0], 
           [3.5, 156.5, 50.5], 
           [-13.0, 141.0, -130.0], 
           [3.0, 191.5, 58.0]]

## The range calibration values for sensor array 5. 
# They will be multiplied to the raw measurements, after adding the offset5. They make a sphere from the 
# ellipsoid. For every sensor are 3 values given, for every axis one. This values also scale your 
# measurements. This mean, you get values in a unit. For example in Gauss.
rangeCalibration5 = [[ 0.00099575,  0.0009305,   0.00088265], 
                     [ 0.00102467,  0.00093375,  0.00104111], 
                     [ 0.00093369,  0.00091987,  0.00103859], 
                     [ 0.00097818,  0.00091086,  0.00102376], 
                     [ 0.00106558,  0.00096779,  0.00111818], 
                     [ 0.00113414,  0.00095098,  0.00124976], 
                     [ 0.00090994,  0.00087295,  0.00100725], 
                     [ 0.00096886,  0.00095015,  0.00099377], 
                     [ 0.00093053,  0.00085657,  0.00100895], 
                     [ 0.00092674,  0.00093712,  0.00101115]]

## The rotation compensation values for sensor array 5. 
# The inverse of them will be multiplied to the measured vector, after multiplying the rangeCalibration5. 
# They ensure, that every sensors orientation is the same. The shape is an array with an quaternion for every sensor.
rotationCompensation5 = [[  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,   0.00000000e+00],
                         [ -1.27040785e-02,   5.17555145e-03,   4.94376317e-03,   9.97185479e-01],
                         [ -1.19883072e-02,   1.69083991e-02,  -1.47537023e-03,   9.94675760e-01],
                         [ -2.11308350e-02,   6.96979458e-03,   6.29574529e-04,   9.94038582e-01],
                         [ -7.33061040e-03,   8.96587575e-03,   1.39615667e-02,   9.91651763e-01],
                         [ -3.82426081e-04,  -1.96004275e-03,   2.02664733e-02,   9.88967943e-01],
                         [ -1.44465036e-02,   9.83496605e-03,   9.50183845e-04,   9.94464088e-01],
                         [  9.89583312e-04,   1.79293041e-03,  -4.98729711e-03,   9.96280834e-01],
                         [ -3.31704351e-03,   5.17878139e-03,   7.15638600e-03,   9.94878834e-01],
                         [ -1.64766482e-02,  -1.28123134e-03,   1.01917177e-02,   9.94985175e-01]]

## The publisher object
pub = rospy.Publisher('magneticMeasurementscompensated', MagneticCalibrated, queue_size=110)

## Calculates the calibrated values from the magneticmeasurementsraw and the calibration values.
# Therefore at first is determined which sensor array sends the data. Than for every Axis of every sensor the 
# value is calibrated. First step is to subtract the offset. After this, the values are scaled with the 
# rangeCalibration values. So if the calibration values are correct, the output values are given in the unit Gauss. 
# The last step is to rotate the vector so, that the coordinate frame is the same like the one of sensor0. This 
# means in an homogeneous magnetic field, the vector of all sensors point in the same direction.
# 
# <b>Input:</b> Data of type "MyMessage"
# 
# <b>Publish:</b> Data of type "MagneticCalibrated" at topic "magneticMeasurementscompensated"
def callback(daten):
    output = MagneticCalibrated()

    if daten.level == 1:                                        # Determine which sensor array made the data.
        dvec = list ()
        for i in range(0,numSensors):                                   # Loop through all sensors of the sensor array
            vec = np.matrix(((0.0, 0.0, 0.0, 1)))
            for j in range(3):                                  # Loop through the 3 axis
                val = float(daten.data[i*3+j])
                val = val - offset1[i][j]                       # Subtract the offset. So the hard iron bias is erased
                val = val*rangeCalibration1[i][j]               # Multiply the rangeCalibration values. So the output values are given in the unit Gauss. The last step is to rotate the vector that the coordinate frame is the same like the one of sensor0. This means in an homogeneous magnetic field, the vector of all sensors point in the same direction.
                vec[0,j] = val
    
            vec = np.transpose(vec)                             # Transform the vector to a format we can deal with
            vec = inv(tf.transformations.quaternion_matrix(rotationCompensation1[i])) * vec     # Rotate the vector that the coordinate frame is the same like the one of sensor0.
    
            mag = Magnetic()                                    # Prepare the output.
            mag.x = vec[0]
            mag.y = vec[1]
            mag.z = vec[2]
            dvec.append(mag)
        output.level= daten.level
        output.data = dvec
        output.header.stamp = rospy.Time.now()
        pub.publish(output)                                     # Publish the calibrated data
        
    if daten.level == 2:                                        # For documentation see calculation for data.level == 1 (line 249 and following)
        dvec = list ()
        for i in range(0,numSensors):
            vec = np.matrix(((0.0, 0.0, 0.0, 1)))
            for j in range(3):
                val = float(daten.data[i*3+j])
                val = val - offset2[i][j]
                val = val*rangeCalibration2[i][j]
                vec[0,j] = val
    
            vec = np.transpose(vec)
            vec = inv(tf.transformations.quaternion_matrix(rotationCompensation2[i])) * vec
    
            mag = Magnetic()
            mag.x = vec[0]
            mag.y = vec[1]
            mag.z = vec[2]
            dvec.append(mag)
        output.level= daten.level
        output.data = dvec
        output.header.stamp = rospy.Time.now()
        pub.publish(output)
        
    if daten.level == 3:                                        # For documentation see calculation for data.level == 1 (line 249 and following)
        dvec = list ()
        for i in range(0,numSensors):
            vec = np.matrix(((0.0, 0.0, 0.0, 1)))
            for j in range(3):
                val = float(daten.data[i*3+j])
                val = val - offset3[i][j]
                val = val*rangeCalibration3[i][j]
                vec[0,j] = val
     
            vec = np.transpose(vec)
            vec = inv(tf.transformations.quaternion_matrix(rotationCompensation3[i])) * vec
     
            mag = Magnetic()
            mag.x = vec[0]
            mag.y = vec[1]
            mag.z = vec[2]
            dvec.append(mag)
        output.level= daten.level
        output.data = dvec
        output.header.stamp = rospy.Time.now()
        pub.publish(output)

    if daten.level == 4:                                        # For documentation see calculation for data.level == 1 (line 249 and following)
        dvec = list ()
        for i in range(0,numSensors):
            vec = np.matrix(((0.0, 0.0, 0.0, 1)))
            for j in range(3):
                val = float(daten.data[i*3+j])
                val = val - offset4[i][j]
                val = val*rangeCalibration4[i][j]
                vec[0,j] = val
    
            vec = np.transpose(vec)
            vec = inv(tf.transformations.quaternion_matrix(rotationCompensation4[i])) * vec
    
            mag = Magnetic()
            mag.x = vec[0]
            mag.y = vec[1]
            mag.z = vec[2]
            dvec.append(mag)
        output.level= daten.level
        output.data = dvec
        output.header.stamp = rospy.Time.now()
        pub.publish(output)

    if daten.level == 5:                                        # For documentation see calculation for data.level == 1 (line 249 and following)
        dvec = list ()
        for i in range(0,numSensors):
            vec = np.matrix(((0.0, 0.0, 0.0, 1)))
            for j in range(3):
                val = float(daten.data[i*3+j])
                val = val - offset5[i][j]
                val = val*rangeCalibration5[i][j]
                vec[0,j] = val
    
            vec = np.transpose(vec)
            vec = inv(tf.transformations.quaternion_matrix(rotationCompensation5[i])) * vec
    
            mag = Magnetic()
            mag.x = vec[0]
            mag.y = vec[1]
            mag.z = vec[2]
            dvec.append(mag)
        output.level= daten.level
        output.data = dvec
        output.header.stamp = rospy.Time.now()
        pub.publish(output)

## listener
# Starts the subscriber.
# Node listen to topic "magneticMeasurementsraw" with the data type "MyMessage"
def listener():
    rospy.Subscriber('magneticMeasurementsraw', MyMessage, callback)
    rospy.spin()


if __name__ == '__main__':
    rospy.init_node('calibration_node', anonymous=True)
    
    try:
    
        listener()
    
    except rospy.ROSInterruptException:
        print "something bad happened..."

