#! /usr/bin/env python

## @package calibration_tool
# The calibration_tool.py is a tool to get the calibration values used in the \link<measurementsraw_to_compensated>measurementsraw_to_compensated\endlink.
# 
# How to use is described at page \ref anwendung "Bedienungsanleitung".
# 
# The script starts a ROS node, which subscribes the "magneticMeasurementsraw" topic. If enough measurements are 
# subscribed it prints out the calibration values you need for the \link<measurementsraw_to_compensated>measurementsraw_to_compensated\endlink script.

import rospy
from magnetic_maps.msg import MyMessage
import numpy as np
import math
from numpy.linalg import inv
import tf
from sensor_msgs.point_cloud2 import PointCloud2
from sensor_msgs.point_cloud2 import PointField
import sensor_msgs.point_cloud2 as pc2

## The publisher object
pub = rospy.Publisher('newPointsAndValues', PointCloud2, queue_size=110)

## The number of sensors on the sensor array you want to calibrate
numSensors = 10

## The number of measurements you take for your calibration.
# 5000 is recommended. The larger numMeasurements is, the longer the calibration takes but the higher is the quality of the calibration.
numMeasurements = 1000

## The scale value define the field strength in your calibration environment.
# You should find out which magnetic field 
# strength exist at your location and set it as scale. As unit you can set what ever you want. 
# In Berlin its around 0.49 Gaus
# 
# if you don't have the field strength at your location you will need a helmholtz coil. 
# Set it to 1 and do the calibration. Than you use 
# the \link<calibration_tool_helmholtz>calibration_tool_helmholtz\endlink script.
# How to do is described in \ref anwendung "Bedienungsanleitung". 
scale = 1

## Filter configuration value
# The maximum difference between two following measurements. Every higher difference could be an error.
filterDistThreshold = 45

## Filter configuration value
# The number of following values out of the filterDistThreshold. To check if its really an error or just an huge jump.
filterNumThreshold = 4

## Rotation configuration value
# The minimum angle to perform a rotation in the calibration.
# Given in Degree.
rotationThreshold = 15

## Saves the received data
measurementData = np.zeros((numMeasurements,30))

## Saves the number of following errors of one sensor.
# The errorFlag of a specific sensor is incremented if the distance between two 
# following measurements is higher than calibration_tool.filterDistThreshold. Its 
# set to 0 if the calibration_tool.jumpFlag was set. See also calibration_tool.callback
errorFlag = np.zeros((1,numSensors))

## Is 1 if the Sensor did a jump instead of an error.
# Is set for a specific sensor if the calibration_tool.errorFlag reaches calibration_tool.filterNumThreshold.
jumpFlag = np.zeros((1,numSensors))

## Number of actual recorded measurements.
i = 0


## Saves the maximum value for the x axis of the sensors
xmax = np.zeros(numSensors)

## Saves the maximum value for the y axis of the sensors
ymax = np.zeros(numSensors)

## Saves the maximum value for the z axis of the sensors
zmax = np.zeros(numSensors)

## Saves the minimum value for the x axis of the sensors
xmin = np.zeros(numSensors)

## Saves the minimum value for the y axis of the sensors
ymin = np.zeros(numSensors)

## Saves the minimum value for the z axis of the sensors
zmin = np.zeros(numSensors)


## Calculates the calibration values for the \link<measurementsraw_to_compensated>measurementsraw_to_compensated\endlink.
# 
# The first step is to filter potential outlier. The new x, y and z data of an sensor is checked against the previous.
# If the difference, for example between the new x value and the old x value, is bigger than 
# calibration_tool.filterDistThreshold, the hole new dataset is rejected and the calibration_tool.errorFlag for the 
# sensor is incremented. If the next datasets have the same potential error it is seen as an jump and the new data 
# is used. How many following datasets must have the jumped value is defined with calibration_tool.filterNumThreshold.
# 
# Than the received data is saved in the calibration_tool.measurementData and the minimum and maximum values are updated.
# Every time new data is received, the values of sensor9 are published as a pointcloud. This is useful to visualize 
# the actual coverage of the sensors measurement space.
# 
# If the desired number of measurements (calibration_tool.numMeasurements) is reached, the offset, the rangeCalibration
# and the rotationCompensation is calculated like described in \ref calibration "Calibration"
# 
# <b>Input:</b> Data of type "MyMessage"
# 
# <b>Publish:</b> Data of type "PointCloud2" at topic "newPointsAndValues"
#
# <b>Print:</b> The calibration values you need for 
# the \link<measurementsraw_to_compensated>measurementsraw_to_compensated\endlink script. They are in the shape you need.
# So just copy and past them!
def callback(daten):
    
    thePoints = []                              # Saves the points of the Pointcloud for visualization

    global scale
    
    global measurementData
    global errorFlag
    global jumpFlag
    global i


    global xmax
    global ymax
    global zmax
    
    global xmin
    global ymin
    global zmin

    if i < numMeasurements:                     # make all the calculation just if we are not done now
        rospy.loginfo(i)                        # just that you have an idea how long to swing your sensors
        for sensor in range(0,numSensors):
            
            # Filter the outliers
            if ((i>1) and (jumpFlag[0,sensor]==0) and ((abs(daten.data[(sensor*3)] - measurementData[i-1,(sensor*3)]) > filterDistThreshold) or (abs(daten.data[((sensor*3)+1)] - measurementData[i-1,((sensor*3)+1)]) > filterDistThreshold) or (abs(daten.data[((sensor*3)+2)] - measurementData[i-1,((sensor*3)+2)]) > filterDistThreshold))):                
                if (errorFlag[0,sensor] <= filterNumThreshold):
                    i = i-1
                    errorFlag[0,sensor] = errorFlag[0,sensor] + 1
                    break
                else:
                    jumpFlag[0,sensor] = 1

            errorFlag[0,sensor] = 0           
            
            if (sensor==(numSensors-1)):
                jumpFlag = np.zeros((1,10))
            
            # Save the new dataset
            measurementData[i, (sensor*3)] = daten.data[(sensor*3)]
            measurementData[i, ((sensor*3)+1)] = daten.data[((sensor*3)+1)]
            measurementData[i, ((sensor*3)+2)] = daten.data[((sensor*3)+2)]

            # update the minimum and maximum values
            if xmax[sensor] < daten.data[(sensor*3)]:
                xmax[sensor] = daten.data[(sensor*3)]
                
            if ymax[sensor] < daten.data[((sensor*3)+1)]:
                ymax[sensor] = daten.data[((sensor*3)+1)]
                
            if zmax[sensor] < daten.data[((sensor*3)+2)]:
                zmax[sensor] = daten.data[((sensor*3)+2)]
            
            if xmin[sensor] > daten.data[(sensor*3)]:
                xmin[sensor] = daten.data[(sensor*3)]
                
            if ymin[sensor] > daten.data[((sensor*3)+1)]:
                ymin[sensor] = daten.data[((sensor*3)+1)]
                
            if zmin[sensor] > daten.data[((sensor*3)+2)]:
                zmin[sensor] = daten.data[((sensor*3)+2)]

            # visualize the values of sensor9 
            if sensor==9:
                pointMagnet = (np.matrix(((float(daten.data[(sensor*3)]), float(daten.data[((sensor*3)+1)]), float(daten.data[((sensor*3)+2)]), float(1)))))/100
                # make a new point in the pointcloud. first three values are XYZ, the following are RGB values
                newPoint = [pointMagnet[0,0], pointMagnet[0,1], pointMagnet[0,2], pointMagnet[0,0], pointMagnet[0,1], pointMagnet[0,2]]
                thePoints.append(newPoint)
                # prepare the pointcloud
                fields = [PointField('x', 0, PointField.FLOAT32, 1),
                          PointField('y', 4, PointField.FLOAT32, 1),
                          PointField('z', 8, PointField.FLOAT32, 1),
                          PointField('r', 12, PointField.FLOAT32, 1),
                          PointField('g', 16, PointField.FLOAT32, 1),
                          PointField('b', 20, PointField.FLOAT32, 1)]  
                newCloud = PointCloud2()
                newCloud.header.frame_id = "camera"
                newCloud = pc2.create_cloud(newCloud.header, fields, thePoints)
                # publish the pointcloud
                pub.publish(newCloud)

    # desired number of measurements reached
    if i == numMeasurements:
        
        # calculate the offsets
        xOffset = (xmin + xmax) / 2
        yOffset = (ymin + ymax) / 2
        zOffset = (zmin + zmax) / 2

        # calculate the rangeCalibration
        xScale = scale / (xmax - xOffset)
        yScale = scale / (ymax - yOffset)
        zScale = scale / (zmax - zOffset)

        
        print("------------Calibration completed------------")
        print("ID of the actual sensor array:")
        print(daten.level)


        # Output in the form you need
        print("offset:")
        print[[xOffset[0], yOffset[0], zOffset[0]],[xOffset[1], yOffset[1], zOffset[1]],[xOffset[2], yOffset[2], zOffset[2]],[xOffset[3], yOffset[3], zOffset[3]],[xOffset[4], yOffset[4], zOffset[4]],[xOffset[5], yOffset[5], zOffset[5]],[xOffset[6], yOffset[6], zOffset[6]],[xOffset[7], yOffset[7], zOffset[7]],[xOffset[8], yOffset[8], zOffset[8]],[xOffset[9], yOffset[9], zOffset[9]]]

        print("rangeCalibration:")
        print[[xScale[0], yScale[0], zScale[0]],[xScale[1], yScale[1], zScale[1]],[xScale[2], yScale[2], zScale[2]],[xScale[3], yScale[3], zScale[3]],[xScale[4], yScale[4], zScale[4]],[xScale[5], yScale[5], zScale[5]],[xScale[6], yScale[6], zScale[6]],[xScale[7], yScale[7], zScale[7]],[xScale[8], yScale[8], zScale[8]],[xScale[9], yScale[9], zScale[9]]]

        # compensate the tilt of the Sensors related to each other
        # Sensor 0 is the leading Sensor which gives the "true" direction
        quaterArray = np.zeros((numSensors, 4))
        for sensor in range(1,numSensors):
            maxAngle = 0.0
            
            # Calibrate the measurements with the previous found values
            for k in range(0,numMeasurements):
                if sensor == 1:
                    # calibrate the first sensor
                    measurementData[k,0] = measurementData[k,0] - xOffset[0]
                    measurementData[k,1] = measurementData[k,1] - yOffset[0]
                    measurementData[k,2] = measurementData[k,2] - zOffset[0]
  
                    measurementData[k,0] = measurementData[k,0] * xScale[0]
                    measurementData[k,1] = measurementData[k,1] * yScale[0]
                    measurementData[k,2] = measurementData[k,2] * zScale[0]

                # Calibrate all measurements
                # Offset
                measurementData[k,sensor*3] = measurementData[k,sensor*3] - xOffset[sensor]
                measurementData[k,(sensor*3)+1] = measurementData[k,(sensor*3)+1] - yOffset[sensor]
                measurementData[k,(sensor*3)+2] = measurementData[k,(sensor*3)+2] - zOffset[sensor]

                # Scale
                measurementData[k,(sensor*3)] = measurementData[k,(sensor*3)] * xScale[sensor]
                measurementData[k,(sensor*3)+1] = measurementData[k,(sensor*3)+1] * yScale[sensor]
                measurementData[k,(sensor*3)+2] = measurementData[k,(sensor*3)+2] * zScale[sensor]


                # calibration of the rotation with the help of quaternion
                # first get the normalized vectors
                vec1 = (np.array(((measurementData[k,0],measurementData[k,1],measurementData[k,2]))))
                vec1 = vec1 / np.linalg.norm(vec1)
                vec2 = (np.array(((measurementData[k,(sensor*3)],measurementData[k,((sensor*3)+1)],measurementData[k,((sensor*3)+2)]))))
                vec2 = vec2 / np.linalg.norm(vec2)

                # dot to get the rotation angle
                cosin = np.dot(vec2, vec1)
                angle = math.acos(cosin)

                # get the quaternion with the biggest angle
                if (angle>maxAngle):
                    maxAngle = angle

                    # Cross to get the rotation axis
                    rotVec = np.cross(vec1, vec2)
                    rotVec = rotVec / np.linalg.norm(rotVec)

                    # check if the angle is too small
                    if (math.degrees(maxAngle) < rotationThreshold):
                        rotQuant = [0,0,0,0]
                    else:
                        # make a rotation quaternion from the angle and the axis
                        rotQuant = tf.transformations.quaternion_about_axis(angle, rotVec)

            quaterArray[sensor,:] = rotQuant[:]

        # print it in the shape you need
        print("rotationCompensation:")
        print[[quaterArray[0,0], quaterArray[0,1], quaterArray[0,2], quaterArray[0,3]],[quaterArray[1,0], quaterArray[1,1], quaterArray[1,2], quaterArray[1,3]],[quaterArray[2,0], quaterArray[2,1], quaterArray[2,2], quaterArray[2,3]],[quaterArray[3,0], quaterArray[3,1], quaterArray[3,2], quaterArray[3,3]],[quaterArray[4,0], quaterArray[4,1], quaterArray[4,2], quaterArray[4,3]],[quaterArray[5,0], quaterArray[5,1], quaterArray[5,2], quaterArray[5,3]],[quaterArray[6,0], quaterArray[6,1], quaterArray[6,2], quaterArray[6,3]],[quaterArray[7,0], quaterArray[7,1], quaterArray[7,2], quaterArray[7,3]],[quaterArray[8,0], quaterArray[8,1], quaterArray[8,2], quaterArray[8,3]],[quaterArray[9,0], quaterArray[9,1], quaterArray[9,2], quaterArray[9,3]]]

    # increment i for the next dataset
    i = i + 1

## listener
# Starts the subscriber.
# Node listen to topic "magneticMeasurementsraw" with the data type "MyMessage"
def listener():
    rospy.Subscriber('magneticMeasurementsraw', MyMessage, callback)
    rospy.spin()


if __name__ == '__main__':
    rospy.init_node('Listen_sensordata', anonymous=True)
    i = 0
    
    try:
    
        listener()
      
    except rospy.ROSInterruptException:
        print "something bad happend while calibration"

